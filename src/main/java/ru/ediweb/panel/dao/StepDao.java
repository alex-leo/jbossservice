package ru.ediweb.panel.dao;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import ru.ediweb.panel.entity.Step;

import java.util.List;

public interface StepDao {
    void add(Step step) throws ConstraintViolationException;
    void update(Step step);
    Step load(String name);
    Step load(Integer id);
    List<Step> loadAll();
    void delete(String name) throws DataIntegrityViolationException;
}
