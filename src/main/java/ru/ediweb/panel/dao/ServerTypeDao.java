package ru.ediweb.panel.dao;

import org.hibernate.exception.ConstraintViolationException;
import ru.ediweb.panel.entity.ServerType;

import java.util.List;

public interface ServerTypeDao {
    void add(ServerType type) throws ConstraintViolationException;
    void update(ServerType type);
    ServerType load(String type);
    ServerType load(Integer id);
    List<ServerType> loadAll();
    void delete(String name);
}
