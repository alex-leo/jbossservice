package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.Command;
import ru.ediweb.panel.entity.CommandStep;
import ru.ediweb.panel.entity.Step;

import java.util.LinkedList;
import java.util.List;

@Repository
public class CommandDaoImpl implements CommandDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    CommandStepDao commandStepDao;

    public CommandDaoImpl() {}

    @Override
    @Transactional
    public void add(Command command) throws ConstraintViolationException{
        sessionFactory.getCurrentSession().save(command);
    }

    @Override
    @Transactional
    public void update(Command command) throws DataIntegrityViolationException {
        sessionFactory.getCurrentSession().update(command);
    }

    @Override
    @Transactional
    public Command load(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Command where name = :name ");
        query.setParameter("name", name);
        Command result = (Command) query.uniqueResult();
        return result;
    }

    @Override
    public Command load(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        return (Command) session.get(Command.class, new Integer(id));
    }

    @Override
    @Transactional
    public List<Command> loadAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Command.class);
        criteria.addOrder(Order.asc("id"));
        List<Command> list = criteria.list();
        return list;
    }

    @Override
    @Transactional
    public void delete(String name) {
        Command command = load(name);
        commandStepDao.cleanPriorities(command);
        sessionFactory.getCurrentSession().delete(command);
    }

    @Override
    @Transactional
    public List<Step> getSteps(String commandName) {
        List<CommandStep> commandSteps = commandStepDao.loadCommandSteps(load(commandName));
        List<Step> steps = new LinkedList<Step>();

        for (CommandStep cs : commandSteps) {
            steps.add(cs.getStep());
        }
        return steps;
    }
}
