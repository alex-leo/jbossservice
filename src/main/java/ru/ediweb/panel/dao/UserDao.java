package ru.ediweb.panel.dao;

import org.hibernate.exception.ConstraintViolationException;
import ru.ediweb.panel.entity.User;

import java.util.List;

public interface UserDao {

    void add(User user) throws ConstraintViolationException;
    void update(User user);
    User load(String login);
    List<User> loadAll();
    void delete(String login);
}


