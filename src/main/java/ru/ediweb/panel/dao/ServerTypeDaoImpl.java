package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.ServerType;

import java.util.List;

@Repository
public class ServerTypeDaoImpl implements ServerTypeDao{

    @Autowired
    private SessionFactory sessionFactory;

    public ServerTypeDaoImpl() {}

    @Override
    @Transactional
    public void add(ServerType type) throws ConstraintViolationException {
        sessionFactory.getCurrentSession().save(type);
    }

    @Override
    @Transactional
    public void update(ServerType type) {
        sessionFactory.getCurrentSession().update(type);
    }

    @Override
    @Transactional
    public ServerType load(String type) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from ServerType where type = :type ");
        query.setParameter("type", type);
        ServerType result = (ServerType) query.uniqueResult();
        return result;
    }

    @Override
    @Transactional
    public ServerType load(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        ServerType serverType = (ServerType) session.get(ServerType.class, new Integer(id));
        return serverType;
    }

    @Override
    @Transactional
    public List<ServerType> loadAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ServerType.class);
        criteria.addOrder(Order.asc("id"));
        List<ServerType> list = criteria.list();
        return list;
    }

    @Override
    @Transactional
    public void delete(String name)  {
        sessionFactory.getCurrentSession().delete(load(name));
    }
}
