package ru.ediweb.panel.dao;

import ru.ediweb.panel.entity.Command;
import ru.ediweb.panel.entity.CommandStep;

import java.util.List;

public interface CommandStepDao {

    void add(CommandStep commandStep);
    List<CommandStep> loadCommandSteps(Command command);

    //Удалить все старые записи приоритетов для комманды
    void cleanPriorities(Command command);

}
