package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.Server;

import java.util.List;

@Repository
public class ServerDaoImpl implements ServerDao {

    @Autowired
    private SessionFactory sessionFactory;

    public ServerDaoImpl() {}

    @Override
    @Transactional
    public void add(Server server) throws ConstraintViolationException {
        sessionFactory.getCurrentSession().save(server);
    }

    @Override
    @Transactional
    public void update(Server server) {
        sessionFactory.getCurrentSession().update(server);
    }

    @Override
    @Transactional
    public Server load(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Server where name = :name ");
        query.setParameter("name", name);
        return (Server) query.uniqueResult();
    }

    @Override
    @Transactional
    public Server load(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Server where id = :id ");
        query.setParameter("id", id);
        return (Server) query.uniqueResult();
    }

    @Override
    @Transactional
    public List<Server> loadAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Server.class);
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }

    @Override
    @Transactional
    public void delete(String name) {
        sessionFactory.getCurrentSession().delete(load(name));
    }
}
