package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.Log;

import java.util.List;

@Repository
public class LogsDaoImpl implements LogsDao {

    @Autowired
    private SessionFactory sessionFactory;

//    @Transactional
//    @Override
//    public List loadAll() {
//        Session session = sessionFactory.getCurrentSession();
//        SQLQuery query = session.createSQLQuery("SELECT * FROM logging_event ORDER BY timestmp");
//        return query.list();
//    }

    @Transactional
    @Override
    public List loadAll() {
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("SELECT * FROM logging_event ORDER BY timestmp");
        query.addEntity(Log.class);
        List<Log> list = query.list();
        return list;
    }

    @Transactional
    @Override
    public List getNewLogs(long lastTimestmp) {
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("SELECT * FROM logging_event WHERE timestmp >" +lastTimestmp + "ORDER BY timestmp");
        query.addEntity(Log.class);
        List<Log> list = query.list();
        return list;
    }
}
