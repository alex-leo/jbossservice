package ru.ediweb.panel.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.Command;
import ru.ediweb.panel.entity.CommandStep;

import java.util.List;

@Repository
public class CommandStepDaoImpl implements CommandStepDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public void add(CommandStep commandStep) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(commandStep);
    }

    @Override
    @Transactional
    public List<CommandStep> loadCommandSteps(Command command) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from CommandStep where command_id = :commandId order by priority");
        query.setParameter("commandId", command.getId());
        //TODO Всегда ли будут упорядоченные значения? не нужен ли LinkedList?
        List<CommandStep> stepsSet = query.list();
        return stepsSet;
    }

    @Override
    @Transactional
    public void cleanPriorities(Command command) {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from CommandStep where command_id = :commandId")
                .setParameter("commandId", command.getId())
                .executeUpdate();
    }
}