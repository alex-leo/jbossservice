package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.Step;

import java.util.List;

@Repository
public class StepDaoImpl implements StepDao {

    @Autowired
    private SessionFactory sessionFactory;

    public StepDaoImpl() {}

    @Override
    @Transactional
    public void add(Step step) throws ConstraintViolationException {
        sessionFactory.getCurrentSession().save(step);
    }

    @Override
    @Transactional
    public void update(Step step) {
        sessionFactory.getCurrentSession().update(step);
    }

    @Override
    @Transactional
    public Step load(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Step where name = :name ");
        query.setParameter("name", name);
        Step step = (Step) query.uniqueResult();
        return step;
    }

    @Override
    @Transactional
    public Step load(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Step step = (Step) session.get(Step.class, new Integer(id));
        return step;
    }

    @Override
    @Transactional
    public List<Step> loadAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Step.class);
        criteria.addOrder(Order.asc("id"));
        List<Step> list = criteria.list();
        return list;
    }

    @Override
    @Transactional
    public void delete(String name) throws DataIntegrityViolationException{
        sessionFactory.getCurrentSession().delete(load(name));
    }

}
