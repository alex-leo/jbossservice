package ru.ediweb.panel.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ediweb.panel.entity.User;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public UserDaoImpl() {}

    @Override
    @Transactional
    public void add(User user) throws ConstraintViolationException {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    @Transactional
    public List<User> loadAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        criteria.addOrder(Order.asc("id"));
        List<User> list = criteria.list();
        return list;
    }

    //TODO добавить вью для подтверждения удаления пользователя
    @Override
    @Transactional
    public void delete(String login) {
        sessionFactory.getCurrentSession().delete(load(login));
    }

    //TODO нужна ли проверка на то, что элемента нет в базе?
    @Override
    @Transactional
    public User load(String login) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from User where login = :login ");
        query.setParameter("login", login);
        User user = (User) query.uniqueResult();
        return user;
    }
}
