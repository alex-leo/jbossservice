package ru.ediweb.panel.dao;

import java.util.List;

public interface LogsDao {
    List loadAll();
    List getNewLogs(long lastTimestmp);
}
