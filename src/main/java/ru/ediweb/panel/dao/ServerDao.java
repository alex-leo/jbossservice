package ru.ediweb.panel.dao;

import org.hibernate.exception.ConstraintViolationException;
import ru.ediweb.panel.entity.Server;

import java.util.List;

public interface ServerDao {
    void add(Server server) throws ConstraintViolationException;
    void update(Server server);
    Server load(String name);
    Server load(int id);
    List<Server> loadAll();
    void delete(String name);
}
