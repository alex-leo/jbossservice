package ru.ediweb.panel.dao;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import ru.ediweb.panel.entity.Command;
import ru.ediweb.panel.entity.Step;

import java.util.List;

public interface CommandDao {
    void add(Command command) throws ConstraintViolationException;
    void update(Command command) throws DataIntegrityViolationException;
    Command load(String name);
    Command load(Integer id);
    List<Command> loadAll();
    void delete(String name);
    List<Step> getSteps(String commandName);
}
