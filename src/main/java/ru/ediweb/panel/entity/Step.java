package ru.ediweb.panel.entity;

import ru.ediweb.panel.entity.enums.StepType;
import ru.ediweb.panel.executor.StepExecutor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "steps")
public class Step {

    private int id;

    @Pattern(regexp = "^[A-Za-z]([.A-Za-z0-9_-]{1,18})([A-Za-z0-9])$", message = "Use letters, numbers, \"- _ .\" to create a type. First symbol need to be a letter, last - letter or number. Length from 3 to 18 symbols")
    private String name;

    @NotNull(message = "Select type")
    private StepType type;


    private String action;

    private Set<CommandStep> commandStep = new HashSet<CommandStep>();

    public Step() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    public StepType getType() {
        return type;
    }

    public void setType(StepType type) {
        this.type = type;
    }

    @Column(name = "action", columnDefinition = "text", length = 1024)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

//    @OneToMany(mappedBy = "step", cascade = CascadeType.ALL)
    @OneToMany(mappedBy = "step")
    public Set<CommandStep> getCommandStep() {
        return commandStep;
    }

    public void setCommandStep(Set<CommandStep> commandStep) {
        this.commandStep = commandStep;
    }

    //Чтобы было красивее записано
    //вместо step.getType().getExecutor().execute(step.getAction)
    //step.getExecutor().execute(step.getAction)
    @Transient
    public StepExecutor getExecutor(){
        return type.getExecutor();
    }

}
