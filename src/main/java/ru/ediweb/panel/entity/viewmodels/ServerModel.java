package ru.ediweb.panel.entity.viewmodels;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ServerModel {

    private static final String IPADDRESS_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @Pattern(regexp = "^[A-Za-z]([.A-Za-z0-9_-]{1,18})([A-Za-z0-9])$", message = "Use letters, numbers, \"- _ .\" to create a type. First symbol need to be a letter, last - letter or number. Length from 3 to 18 symbols")
    private String name;

    private String oldName;

    @Pattern(regexp = IPADDRESS_PATTERN, message = "IP-ddress is not valid")
    private String host;

    @Size(min = 1, message = "Insert path to JBOSS_HOME")
    private String path;

    @Min(value = 1, message = "Select type")
    private int typeId;

    public ServerModel() {}

    public ServerModel(String name, String host, String path, int typeId) {
        this.name = name;
        this.oldName = name;
        this.host = host;
        this.path = path;
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}
