package ru.ediweb.panel.entity.viewmodels;

import javax.validation.constraints.Pattern;

public class CommandModel {

    @Pattern(regexp = "^[A-Za-z]([.A-Za-z0-9_-]{1,18})([A-Za-z0-9])$", message = "Use letters, numbers, \"- _ .\" to create command name. First symbol need to be a letter, last - letter or number. Length from 3 to 18 symbols")
    private String name;
    private boolean hasParameters;
    private String parameters;
    private String steps;

    public CommandModel() {
    }

    public CommandModel(String name, boolean hasParameters, String parameters,  String steps) {
        this.name = name;
        this.hasParameters = hasParameters;
        this.parameters = parameters;
        this.steps = steps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasParameters() {
        return hasParameters;
    }

    public void setHasParameters(boolean hasParameters) {
        this.hasParameters = hasParameters;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }
}
