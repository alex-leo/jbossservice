package ru.ediweb.panel.entity.enums;

import ru.ediweb.panel.executor.LocalStepExecutor;
import ru.ediweb.panel.executor.RemoteStepExecutor;
import ru.ediweb.panel.executor.StepExecutor;

/**
 * Created by alex on 10.06.15.
 */
public enum StepType {

    LOCAL {
        @Override
        public StepExecutor getExecutor() {
            return new LocalStepExecutor();
        }
    },
    REMOTE {
        @Override
        public StepExecutor getExecutor() {
            return new RemoteStepExecutor();
        }
    };

    public abstract StepExecutor getExecutor();

}
