package ru.ediweb.panel.entity.enums;

public enum Role {
    ADMIN,
    DEPLOYER,
    WATCHER;

    Role() {
    }
}
