package ru.ediweb.panel.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "server_types")
public class ServerType {

    private int id;

    @Pattern(regexp = "^[A-Za-z]([.A-Za-z0-9_-]{1,18})([A-Za-z0-9])$", message = "Use letters, numbers, \"- _ .\" to create a type. First symbol need to be a letter, last - letter or number. Length from 3 to 18 symbols")
    private String type;
    private Set<Server> serverSet = new HashSet<Server>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @OneToMany(mappedBy = "type")
    public Set<Server> getServerSet() {
        return serverSet;
    }

    public void setServerSet(Set<Server> serverSet) {
        this.serverSet = serverSet;
    }
}
