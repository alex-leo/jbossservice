package ru.ediweb.panel.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "commands")
public class Command {

    private int id;

    private String name;

    private boolean hasParameters;

    private Set<CommandStep> commandStep = new LinkedHashSet<CommandStep>();

    public Command() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasParameters() {
        return hasParameters;
    }

    public void setHasParameters(boolean hasParameters) {
        this.hasParameters = hasParameters;
    }

    @OneToMany(mappedBy = "command")
    public Set<CommandStep> getCommandStep() {
        return commandStep;
    }

    public void setCommandStep(Set<CommandStep> commandStep) {
        this.commandStep = commandStep;
    }
}
