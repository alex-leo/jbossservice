package ru.ediweb.panel.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Random;

public class PaswordGenerator {

    private final static int PASSWORD_LENGTH = 6;

    public static String generate(){
        char[] password = new char[PASSWORD_LENGTH];
        String validChars = "abcdefghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ1234567890";

        for (int i = 0; i <PASSWORD_LENGTH; i++) {
            password[i] = validChars.charAt(new Random().nextInt(validChars.length()));
        }
        return new String(password);
    }

    public static String encodeBCryptPassword(String password){
        return new BCryptPasswordEncoder().encode(password);
    }
}
