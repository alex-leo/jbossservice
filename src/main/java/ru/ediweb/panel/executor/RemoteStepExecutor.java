package ru.ediweb.panel.executor;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.entity.Step;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class RemoteStepExecutor implements StepExecutor {

    private final static Logger LOGGER = LoggerFactory.getLogger(StepExecutor.class);
    Session session;

    @Override
    public void execute(int commandId, Step step, Server server) {

        String hostname = server.getHost();

        File tmpFile = null;
        try {
            //Создать файл из шага
            tmpFile = File.createTempFile("step_", "");
            tmpFile.setExecutable(true);

            PrintWriter out = new PrintWriter(tmpFile);
            out.println(step.getAction());
            out.close();

            //Загрузить файл на сервер

            JSch jsch = new JSch();
            session = jsch.getSession("sm_alexeyl", hostname,22);
            jsch.addIdentity("/home/alex/.ssh/id_rsa");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            ChannelSftp channelSftp = (ChannelSftp)session.openChannel("sftp");
            channelSftp.connect();
            channelSftp.cd("/tmp/111");
            channelSftp.put(new FileInputStream(tmpFile),tmpFile.getName());
            channelSftp.disconnect();
            LOGGER.info("File " + tmpFile.getName() + " uploaded");

//          Выполнить файл на сервере
            final ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
            channelExec.setCommand("sh /tmp/111/" + tmpFile.getName());
            LOGGER.info("PERED", commandId);
            channelExec.connect();

            Scanner sc = new Scanner(channelExec.getInputStream());
            while (sc.hasNextLine()) {
                LOGGER.info(sc.nextLine(), commandId);
            }

            channelExec.disconnect();

            LOGGER.info("POSLE", commandId);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        } finally {
            session.disconnect();
//            tmpFile.delete();
        }

    }

    @Override
    public void stop() {
        session.disconnect();
    }
}
