package ru.ediweb.panel.executor;

import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//@Scope(value = "prototype")
public class RunCommandHelper {

    private Map<String, Process> processes = new HashMap<String, Process>();

    public void startProcess(String id, Process process) {
        processes.put(id, process);
        process.run();
        processes.remove(id);
    }

    public Set<String> getAllIds() {
        return processes.keySet();
    }



    public Process getProcess(String id) {
        return processes.get(id);
    }


    public static interface Process {
        void run();
        void stop();
        boolean isAlive();
    }
}
