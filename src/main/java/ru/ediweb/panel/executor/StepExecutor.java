package ru.ediweb.panel.executor;

import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.entity.Step;

import java.io.File;

public interface StepExecutor {
    void execute(int commandId, Step step, Server server);
    void stop();
}
