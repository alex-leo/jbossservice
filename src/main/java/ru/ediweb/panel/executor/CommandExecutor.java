package ru.ediweb.panel.executor;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.ediweb.panel.dao.CommandDao;
import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.entity.Step;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Service
@Scope("prototype")
public class CommandExecutor {

    @Autowired
    CommandDao commandDao;

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CommandExecutor.class);

    private StepExecutor currentExecutor;
    private boolean active = true;

    public void execute(String commandName, List<Server> servers){
        active = true;
        LOGGER.info("START EXECUTING");

        List<Step> steps = commandDao.getSteps(commandName);
        int commandId = commandDao.load(commandName).getId();

        Iterator<Server> serverIterator = servers.iterator();
        while (active && serverIterator.hasNext()) {
            LOGGER.info("IN 1ST WHILE");
            Server server = serverIterator.next();

            Iterator<Step> stepIterator = steps.iterator();
            while (active && stepIterator.hasNext()) {
                LOGGER.info("In 2ND WHILE");
                Step step = stepIterator.next();
                currentExecutor = step.getExecutor();
                currentExecutor.execute(commandId, step, server); //передается id комманды, для того чтобы записать его в лог
            }
        }
        LOGGER.info("EXIT EXECUTE");
    }

    public void stop() {
        active = false;
        currentExecutor.stop();
    }

}
