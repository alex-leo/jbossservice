package ru.ediweb.panel.executor;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.entity.Step;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class LocalStepExecutor implements StepExecutor {

    private final static Logger LOGGER = LoggerFactory.getLogger(StepExecutor.class);
    Process process;

    @Override
    public void execute(int commandId, Step step, Server server) {

        File tmpFile = null;
        try {
            tmpFile = File.createTempFile("step_", "");
            tmpFile.setExecutable(true);

            PrintWriter out = new PrintWriter(tmpFile);
            out.println(step.getAction());
            out.close();

            ProcessBuilder pb = new ProcessBuilder(tmpFile.getAbsolutePath());
            pb.redirectErrorStream(true);
            process = pb.start();
            inheritIO(commandId, process.getInputStream());
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
//            tmpFile.delete();
        }

    }

    @Override
    public void stop() {
        process.destroy();
    }

    private void inheritIO(final int commandId, final InputStream src) {
        new Thread(new Runnable() {
            public void run() {
                Scanner sc = new Scanner(src);
                while (sc.hasNextLine()) {
                    LOGGER.info(sc.nextLine(), commandId);
                }
            }
        }).start();
    }

}
