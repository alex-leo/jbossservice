package ru.ediweb.panel.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.ediweb.panel.entity.enums.Role;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder())
                .usersByUsernameQuery(
                        "select login, password, enabled from users where login=?")
                .authoritiesByUsernameQuery(
                        "select login, role from users where login=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //TODO нужно ли прикрутить токены?
        http.csrf().disable()
             .authorizeRequests()
                .antMatchers("/users/*/change_password").permitAll()
                .antMatchers("/users/**", "/servers/**", "/commands/**", "/steps/**").hasAuthority(Role.ADMIN.name())
                .antMatchers("/resources/**").permitAll()
                .anyRequest().authenticated();

        http
           .formLogin()
                .loginPage("/login")
                .failureUrl("/login?auth=failed")
                .permitAll()
                .and()
           .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login");




        http.exceptionHandling().accessDeniedPage("/403");
    }

}
