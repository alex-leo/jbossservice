package ru.ediweb.panel.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.ediweb.panel.dao.CommandDao;
import ru.ediweb.panel.dao.ServerDao;
import ru.ediweb.panel.dao.ServerTypeDao;
import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.entity.viewmodels.ServerModel;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ServerController {

    //Views
    private final String INDEX_PAGE = "index_page";
    private final String LIST = "servers/index";
    private final String REDIRECT = "redirect:/servers";
    private final String FORM = "servers/form";


    @Autowired
    ServerDao serverDao;
    @Autowired
    ServerTypeDao serverTypeDao;
    @Autowired
    CommandDao  commandDao;

    //Главная страница. Возможно нужно будет отображать совсем другие данные
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String deployPage(ModelMap modelMap) {
        modelMap.addAttribute("servers", serverDao.loadAll());
        modelMap.addAttribute("commands", commandDao.loadAll());
        modelMap.addAttribute("servers", serverDao.loadAll());
        return INDEX_PAGE;
    }

    @RequestMapping(value = "/servers", method = RequestMethod.GET)
    public String loadAll(ModelMap modelMap) {
        modelMap.addAttribute("servers", serverDao.loadAll());
        return LIST;
    }

    @RequestMapping(value = "/servers/add", method = RequestMethod.GET)
    public String goToAddForm(ModelMap modelMap) {
        modelMap.addAttribute("actionAdd", true);
        modelMap.addAttribute("types", serverTypeDao.loadAll());
        modelMap.addAttribute("server", new ServerModel());
        return FORM;
    }

    @RequestMapping(value = "/servers/add", method = RequestMethod.POST)
    public String add(ModelMap modelMap,
                      @Valid @ModelAttribute("server") ServerModel serverModel,
                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("actionAdd", true);
            modelMap.addAttribute("types", serverTypeDao.loadAll());
            return FORM;
        }

        Server server = new Server(serverModel.getName(), serverModel.getHost(), serverModel.getPath(), serverTypeDao.load(serverModel.getTypeId()));

        try {
            serverDao.add(server);
            return REDIRECT;
        } catch (ConstraintViolationException e) {
            bindingResult.rejectValue("name", "server", "Server with name \"" + server.getName() + "\" allready exits");
            modelMap.addAttribute("actionAdd", true);
            modelMap.addAttribute("types", serverTypeDao.loadAll());
            return FORM;
        }
    }

    @RequestMapping(value = "/servers/{name}/edit", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @PathVariable("name") String name) {

        Server server = serverDao.load(name);
        ServerModel sm = new ServerModel(name, server.getHost(), server.getPath(), server.getType().getId());

        modelMap.addAttribute("server", sm);
        modelMap.addAttribute("types", serverTypeDao.loadAll());
        return FORM;
    }

    @RequestMapping(value = "/servers/{name}/update", method = RequestMethod.POST)
    public String update(ModelMap modelMap,
                         @PathVariable("name") String name,
                         @Valid @ModelAttribute("server") ServerModel serverModel,
                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("types", serverTypeDao.loadAll());
            return FORM;
        }

        Server server = serverDao.load(name);
        server.setName(serverModel.getName());
        server.setHost(serverModel.getHost());
        server.setType(serverTypeDao.load(serverModel.getTypeId()));
        server.setPath(serverModel.getPath());

        try {
            serverDao.update(server);
            return REDIRECT;
        } catch (DataIntegrityViolationException e) {
            bindingResult.rejectValue("name", "server", "Server with name \"" + server.getName() + "\" allready exits");
            serverModel.setName(name);
//            modelMap.addAttribute("server", serverModel);
            modelMap.addAttribute("types", serverTypeDao.loadAll());
            return FORM;
        }
    }

    @RequestMapping(value = "/servers/{name}/delete")
    public ModelAndView deleteServer(@PathVariable("name") String name){
        serverDao.delete(name);
        return new ModelAndView(REDIRECT);
    }
}
