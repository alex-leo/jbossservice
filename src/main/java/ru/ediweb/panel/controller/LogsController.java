package ru.ediweb.panel.controller;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.ediweb.panel.dao.LogsDao;
import ru.ediweb.panel.entity.Log;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class LogsController {

    @Autowired
    private LogsDao logsDao;
    private final static Logger LOGGER = LoggerFactory.getLogger(CommandExecutorController.class);

    @RequestMapping(value = "/logs", method = RequestMethod.GET)
    public String logs() { return "/logs"; }

    @RequestMapping(value = "/get_new_logs", method = RequestMethod.POST)
    public @ResponseBody
    String getNewLogs(@RequestParam(value = "lastTimestmp") long oldLastTimestmp, HttpServletResponse response) {

        LOGGER.info("GET NEW " + oldLastTimestmp);
        List<Log> logs = logsDao.getNewLogs(oldLastTimestmp);

        String json = new Gson().toJson(logs);

        return json;

    }








    private long getLastTimestmp(List<Log> l){
        int lastIndex = l.size() - 1;
        return l.get(lastIndex).getTimestmp();
    }
}
