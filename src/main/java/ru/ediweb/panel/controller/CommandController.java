package ru.ediweb.panel.controller;

import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.ediweb.panel.dao.CommandDao;
import ru.ediweb.panel.dao.CommandStepDao;
import ru.ediweb.panel.dao.StepDao;
import ru.ediweb.panel.entity.Command;
import ru.ediweb.panel.entity.viewmodels.CommandModel;
import ru.ediweb.panel.entity.CommandStep;
import ru.ediweb.panel.entity.Step;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CommandController {

    //Views
    private final String FORM = "commands/form";
    private final String LIST = "commands/index";
    private final String REDIRECT = "redirect:/commands";

    private final static Logger LOGGER = LoggerFactory.getLogger(CommandController.class);

    @Autowired SessionFactory sessionFactory;
    @Autowired CommandDao commandDao;
    @Autowired StepDao stepDao;
    @Autowired CommandStepDao commandStepDao;

    @RequestMapping(value = "/commands", method = RequestMethod.GET)
    public String loadAll(ModelMap modelMap) {
        modelMap.addAttribute("commands", commandDao.loadAll());
        return LIST;
    }

    @RequestMapping(value = "/commands/add", method = RequestMethod.GET)
    public String goToAddForm(ModelMap modelMap) {
        addAttrsToModel(modelMap, new CommandModel());
        return FORM;
    }

    @RequestMapping(value = "/commands/add", method = RequestMethod.POST)
    public String add(final ModelMap modelMap,
                      @Valid @ModelAttribute("commandModel")CommandModel commandModel,
                      BindingResult bindingResult) {

        LOGGER.info(String.valueOf(commandModel.isHasParameters()));

        if (bindingResult.hasErrors()) {
            addAttrsToModel(modelMap, commandModel);
            return FORM;
        }

        Command command = new Command();
        command.setName(commandModel.getName());
        command.setHasParameters(commandModel.isHasParameters());
        try {
            commandDao.add(command);
        } catch (ConstraintViolationException e) {
            bindingResult.rejectValue("name", "commandModel", "Command \"" + commandModel.getName() + "\" allready exits");
            addAttrsToModel(modelMap, commandModel);
            return FORM;
        }

        if (!commandModel.getSteps().equals("")){
            String [] orderedStepId = commandModel.getSteps().split(",");
            for (int i = 0; i < orderedStepId.length; i++) {
                CommandStep commandStep = new CommandStep();
                commandStep.setCommand(command);
                commandStep.setStep(stepDao.load(new Integer(Integer.parseInt(orderedStepId[i]))));
                commandStep.setPriority(i+1);
                commandStepDao.add(commandStep);
            }
        }
        return REDIRECT;
    }

    private void addAttrsToModel(ModelMap modelMap, CommandModel commandModel) {
        modelMap.addAttribute("commandModel", commandModel);
        modelMap.addAttribute("steps", stepDao.loadAll());
        modelMap.addAttribute("actionAdd", true);
    }

    @RequestMapping(value = "commands/{name}/edit", method = RequestMethod.GET)
    public String editCommand(ModelMap modelMap, @PathVariable("name") String name) {

        //Получить список шагов и сформировать из них строку
        List<Step> stepList = commandDao.getSteps(name);
        StringBuilder steps = new StringBuilder(stepList.size());
        for (Step step : stepList) {
            if (steps.length() > 0) steps.append(",");
            steps.append(step.getId());
        }
        modelMap.addAttribute("commandModel", new CommandModel(name, commandDao.load(name).isHasParameters(), null,steps.toString()));
        modelMap.addAttribute("steps", stepDao.loadAll());
        return FORM;
    }

    @RequestMapping(value = "/commands/{name}/update", method = RequestMethod.POST)
    public String update(ModelMap modelMap,
                         @PathVariable("name") String name,
                         @Valid @ModelAttribute("commandModel") CommandModel commandModel,
                         BindingResult bindingResult) {

        LOGGER.info("CM" + commandModel.getName());

        if (bindingResult.hasErrors()){
            commandModel.setName(name);
            modelMap.addAttribute("commandModel", commandModel);
            modelMap.addAttribute("steps", stepDao.loadAll());
            return FORM;
        }

        Command command = commandDao.load(name);
        command.setName(commandModel.getName());
        command.setHasParameters(commandModel.isHasParameters());

        try {
            commandDao.update(command);
        } catch (DataIntegrityViolationException e) {
            bindingResult.rejectValue("name", "commandModel", "Command \"" + commandModel.getName() + "\" allready exits");
            commandModel.setName(name);
            modelMap.addAttribute("commandModel", commandModel);
            modelMap.addAttribute("steps", stepDao.loadAll());
            return FORM;
        }

        commandStepDao.cleanPriorities(command);
        LOGGER.info("DDDDDDD " + commandModel.getSteps());
        if (!commandModel.getSteps().equals("")){
            String [] orderedStepId = commandModel.getSteps().split(",");
            for (int i = 0; i < orderedStepId.length; i++) {
                CommandStep commandStep = new CommandStep();
                commandStep.setCommand(command);
                commandStep.setStep(stepDao.load(new Integer(Integer.parseInt(orderedStepId[i]))));
                commandStep.setPriority(i+1);
                commandStepDao.add(commandStep);
            }
        }
        return REDIRECT;
    }

    @RequestMapping(value = "/commands/{name}/delete")
    public String deleteServer(@PathVariable("name") String name){
        commandDao.delete(name);
        return REDIRECT;
    }

    @RequestMapping(value = "/commands/{name}/execute")
    public String execute(@PathVariable("name") String name) {



        return "redirect:/logs";
    }
}