package ru.ediweb.panel.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.omg.CORBA.ServerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.ediweb.panel.dao.StepDao;
import ru.ediweb.panel.entity.Step;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@Controller
public class StepController {

    @Autowired
    StepDao stepDao;

    @RequestMapping(value = "/steps", method = RequestMethod.GET)
    public String loadAll(ModelMap modelMap) {
        modelMap.addAttribute("steps", stepDao.loadAll());
        return "steps/index";
    }

    @RequestMapping(value = "/steps/add", method = RequestMethod.GET)
    public String goToAddForm(ModelMap modelMap) {
        modelMap.addAttribute("actionAdd", true);
        modelMap.addAttribute("step", new Step());
        return "steps/form";
    }

    @RequestMapping(value = "/steps/add", method = RequestMethod.POST)
    public String add(ModelMap modelMap,
                      @Valid @ModelAttribute("step") Step step,
                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("actionAdd", true);
            return "steps/form";
        }

        try {
            step.setAction(step.getAction().replace("\r", ""));
            stepDao.add(step);
            return "redirect:/steps";
        } catch (ConstraintViolationException e) {
            bindingResult.rejectValue("name", "step", "Step with name \"" + step.getName() + "\" allready exits");
            modelMap.addAttribute("actionAdd", true);
            return "steps/form";
        }

    }

    @RequestMapping(value = "/steps/{name}/edit", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @PathVariable("name") String name) {
        modelMap.addAttribute("step", stepDao.load(name));
        return "steps/form";
    }

    @RequestMapping(value = "/steps/{name}/update", method = RequestMethod.POST)
    public String update(ModelMap modelMap,
                         @PathVariable ("name") String name,
                         @Valid @ModelAttribute("step") Step step,
                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            step.setName(name);
            return "steps/form";
        }

        try {
            step.setAction(step.getAction().replace("\r", ""));
            stepDao.update(step);
            return "redirect:/steps";
        } catch (DataIntegrityViolationException e) {
            bindingResult.rejectValue("name", "step", "Step with name \"" + step.getName() + "\" allready exits");
            step.setName(name);
            return "steps/form";
        }
    }

    @RequestMapping(value = "/steps/{name}/delete", method = RequestMethod.GET)
    public String delete(ModelMap modelMap, @PathVariable("name") String name){
        try {
            stepDao.delete(name);
            return "redirect:/steps";
        } catch (DataIntegrityViolationException e){
            modelMap.addAttribute("failedMessage", "Сначала удалите шаг из всех комманд, в которые он входит!");
            modelMap.addAttribute("targetPage", "/steps");
            return "notification";
        }
    }
}
