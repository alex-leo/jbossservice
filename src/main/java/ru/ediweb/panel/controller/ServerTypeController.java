package ru.ediweb.panel.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.ediweb.panel.dao.ServerTypeDao;
import ru.ediweb.panel.entity.ServerType;

import javax.validation.Valid;

@Controller
public class ServerTypeController {

    @Autowired
    ServerTypeDao serverTypeDao;

    @RequestMapping(value = "/server_types", method = RequestMethod.GET)
    public String loadAll(ModelMap modelMap) {
        modelMap.addAttribute("types", serverTypeDao.loadAll());
        return "servertypes/index";
    }

    @RequestMapping(value = "/server_types/add", method = RequestMethod.GET)
    public String goToAddForm(ModelMap modelMap) {
        modelMap.addAttribute("type", new ServerType());
        modelMap.addAttribute("actionAdd", true);
        return "servertypes/form";
    }

    @RequestMapping(value = "/server_types/add", method = RequestMethod.POST)
    public String add(ModelMap modelMap,
                      @Valid @ModelAttribute("type") ServerType type,
                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("actionAdd", true);
            return "servertypes/form";
        }

        try {
            serverTypeDao.add(type);
            return "redirect:/server_types";
        } catch (ConstraintViolationException e) {
            modelMap.addAttribute("actionAdd", true);
            modelMap.addAttribute(type);
            bindingResult.rejectValue("type", "type", "Type \"" + type.getType() + "\" allready exits");
            return "servertypes/form";
        }

    }

    @RequestMapping(value = "/server_types/{type}/edit")
    public String edit(ModelMap modelMap, @PathVariable("type") String type) {
        modelMap.addAttribute("type", serverTypeDao.load(type));
        return "servertypes/form";
    }

    @RequestMapping(value = "/server_types/{type}/update", method = RequestMethod.POST)
    public String update(ModelMap modelMap,
                         @PathVariable("type") String type,
                         @Valid @ModelAttribute("type") ServerType s,
                         BindingResult bindingResult) {

        if (bindingResult.hasErrors()){
            s.setType(type);
            return "servertypes/form";
        }

        ServerType serverType = serverTypeDao.load(type);
        serverType.setType(s.getType());

        try{
            serverTypeDao.update(serverType);
            return "redirect:/server_types";
        } catch (DataIntegrityViolationException e){
            bindingResult.rejectValue("type", "type", "Server type \"" + s.getType() + "\" allready exits");
            s.setType(type);
            modelMap.addAttribute("type", s);
            return "servertypes/form";
        }
    }

    @RequestMapping(value = "/server_types/{type}/delete")
    public String delete(@PathVariable("type") String type) {
        serverTypeDao.delete(type);
        return "redirect:/server_types";
    }
}
