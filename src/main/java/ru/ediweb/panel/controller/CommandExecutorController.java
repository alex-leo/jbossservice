package ru.ediweb.panel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ediweb.panel.dao.ServerDao;
import ru.ediweb.panel.entity.Server;
import ru.ediweb.panel.executor.CommandExecutor;
import ru.ediweb.panel.executor.RunCommandHelper;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ru.ediweb.panel.executor.RunCommandHelper.Process;

@Controller
public class CommandExecutorController {
    @Autowired
    ServerDao serverDao;
    @Autowired
    RunCommandHelper helper;
    @Resource(lookup = "java:comp/env/jdbc/jboss_service")
    private DataSource ds;
    private final static Logger LOGGER = LoggerFactory.getLogger(CommandExecutorController.class);
    Thread t;

    @Lookup
    public CommandExecutor getCommExecutor(){
        return null;
    }

//    @RequestMapping(value = "/test", method = RequestMethod.GET)
//    public String test() {
//
//        servers = null;
//        servers =  new ArrayList<Server>();
//        servers.add(serverDao.load("test"));
//
//       Thread t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                final CommandExecutor commandExecutor = getCommExecutor();
//                helper.startProcess(new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()), new Process() {
//
//                    @Override
//                    public void run() {
//                        commandExecutor.execute("command1", servers);
//                    }
//
//                    @Override
//                    public void stop() {
//                        commandExecutor.stop();
//                    }
//
//                    @Override
//                    public boolean isAlive() {
//                        return false;
//                    }
//                });
//            }
//        });
//        t.start();
//
//        LOGGER.info("AFTER START");
//        return "redirect:/logs";
//    }

    @RequestMapping(value = "/execute", method = RequestMethod.POST)
    public String executeCommand(@RequestParam("servers") String serversFromView,
                                 @RequestParam("command") final String command,
                                 @RequestParam("parameters") final String parameters){

        //Получаем массив c id выбранных серверов
        String[] serversArrString = serversFromView.split(",");

        //Получаем список выбранных серверов
        final List<Server> serversList = new ArrayList<>(serversArrString.length);
        for (String s : serversArrString){
            Server server = serverDao.load(Integer.parseInt(s));
            serversList.add(server);
        }

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                final CommandExecutor commandExecutor = getCommExecutor();
                helper.startProcess(new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()), new Process() {

                    @Override
                    public void run() {
                        commandExecutor.execute(command, serversList);
                    }

                    @Override
                    public void stop() {
                        commandExecutor.stop();
                    }

                    @Override
                    public boolean isAlive() {
                        return false;
                    }
                });
            }
        });
        t.start();

        return "redirect:/logs";
    }
}