package ru.ediweb.panel.controller;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.ediweb.panel.executor.RunCommandHelper;

import java.util.Set;

@Controller
public class TasksController {

    @Autowired
    RunCommandHelper helper;

    private final static Logger LOGGER = LoggerFactory.getLogger(TasksController.class);

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String tasks(ModelMap modelMap) {
        return "/tasks";
    }

    @RequestMapping(value = "/refresh_tasks", method = RequestMethod.POST)
    public @ResponseBody
    String refreshTasks(){
        LOGGER.info("IN TASKS REFRESH");
        Set<String> tasks = helper.getAllIds();
        String json = new Gson().toJson(tasks);
        return json;
    }

    @RequestMapping(value = "/tasks/{taskId}/stop")
    public String stopTask(@PathVariable(value = "taskId") String taskId) {
        LOGGER.info("PROCESS_ID " + taskId);
        LOGGER.info("ALL " + helper.getAllIds());
        helper.getProcess(taskId).stop();
        return "redirect:/tasks";
    }
}
