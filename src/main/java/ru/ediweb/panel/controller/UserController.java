package ru.ediweb.panel.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ediweb.panel.dao.UserDao;
import ru.ediweb.panel.entity.User;
import ru.ediweb.panel.util.PaswordGenerator;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    UserDao userDao;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String loadAll(ModelMap modelMap){
        modelMap.addAttribute("users", userDao.loadAll());
        return "users/index";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String goToAddForm(ModelMap modelMap) {
        modelMap.addAttribute("user", new User());
        modelMap.addAttribute("actionAdd", true);
        return "users/form";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String add(ModelMap modelMap,
                      @Valid @ModelAttribute("user") User user,
                      BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("actionAdd", true);
            return "users/form";
        }

        String password = PaswordGenerator.generate();
        String passwordEncoded = PaswordGenerator.encodeBCryptPassword(password);
        user.setPassword(passwordEncoded);

        try {
            userDao.add(user);
            modelMap.addAttribute("targetPage", "/users");
            modelMap.addAttribute("successMessage", "User is added. Password: " + password);
            return "notification";
        } catch (ConstraintViolationException e) {
            modelMap.addAttribute("user", user);
            modelMap.addAttribute("actionAdd", true);
            bindingResult.rejectValue("login", "user", "User with login \"" + user.getLogin() + "\" allready exits");
            return "users/form";
        }
    }

    @RequestMapping(value = "/users/{login}/edit", method = RequestMethod.GET)
    public String edit(ModelMap modelMap, @PathVariable("login") String login) {
        modelMap.addAttribute("user", userDao.load(login));
        return "users/form";
    }

    @RequestMapping(value = "/users/{login}/update", method = RequestMethod.POST)
    public String update(@PathVariable("login") String login, @ModelAttribute("user") User user) {
        user.setPassword(userDao.load(login).getPassword());
        userDao.update(user);
        return "redirect:/users";
    }

    @RequestMapping(value = "/users/{login}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable("login") String login){
        userDao.delete(login);
        return "redirect:/users";
    }

    @RequestMapping(value = "/users/{login}/reset_password", method = RequestMethod.GET)
    public String resetPassword(ModelMap modelMap, @PathVariable("login") String login){
        String password = PaswordGenerator.generate();
        String passwordEncoded = PaswordGenerator.encodeBCryptPassword(password);
        User user = userDao.load(login);
        user.setPassword(passwordEncoded);
        userDao.update(user);
        modelMap.addAttribute("newPassword", "New password: " + password);
        modelMap.addAttribute("targetPage", "/users");
        return "notification";
    }

    @RequestMapping(value = "/users/{login}/change_password", method = RequestMethod.GET)
    public String changePasswordGet(ModelMap modelMap, @PathVariable("login") String login) {
        modelMap.addAttribute("login", login);
        return "/users/change_password";
    }

    @RequestMapping(value = "/users/{login}/change_password", method = RequestMethod.POST)
    public String changePasswordPost(ModelMap modelMap,
                                     @RequestParam("currentPassword") String currentPassword,
                                     @RequestParam("password1") String password1,
                                     @RequestParam("password2") String password2) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User user = userDao.load(login);

        boolean failFlag = false;

        if ((!encoder.matches(currentPassword, user.getPassword()))) {
            modelMap.addAttribute("wrongCurrentPassword", "Wrong current password");
            failFlag = true;
        }
        if (!password1.equals(password2)){
            modelMap.addAttribute("wrongConfirmPassword", "Wrong confirm password");
            failFlag = true;
        }
        if (password1.length() < 6) {
            modelMap.addAttribute("wrongPasswordLength", "Password length too short");
            failFlag = true;
        }
        if (failFlag){
            modelMap.addAttribute("login", login);
            return "/users/change_password";
        }

        user.setPassword(encoder.encode(password1));
        userDao.update(user);
        modelMap.addAttribute("password_reset_status", "Password has been updated successfully");
        modelMap.addAttribute("targetPage", "/users");
        return "notification";
    }
}
