<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Edit server type</small></p></h2>
        </div>

        <jsp:include page="../header.jsp" />
    </div>

    <div class="row center-block col-md-12">
        <c:choose>
            <c:when test="${actionAdd == true}">
                <c:set var="action" value="${pageContext.request.contextPath}/server_types/add"/>
            </c:when>
            <c:otherwise>
                <c:set var="action" value="${pageContext.request.contextPath}/server_types/${type.type}/update"/>
            </c:otherwise>
        </c:choose>

        <c:out value="${type.type}"/>

        <form:form class="form-horizontal" method="post" modelAttribute="type" action="${action}">
            <div class="form-group">
                <form:hidden path="id"/>
                <label class="col-sm-4 control-label">Type</label>
                <div class="col-sm-4">
                    <form:input path="type" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2">
                    <form:errors path="type" cssClass="alert alert-danger error-validation-message" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-3">
                    <form:button type="submit" class="btn btn-primary btn-sm btn-success">Save</form:button>
                    <a href="${pageContext.request.contextPath}/server_types/" class="btn btn-primary btn-sm btn-danger">Cancel</a>
                </div>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>
