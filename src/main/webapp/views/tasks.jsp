<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dateObject" class="java.util.Date" />

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
</head>
<body>

<div class="container" id="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Tasks</small></p></h2>
        </div>

        <jsp:include page="header.jsp" />

        <table id="table_tasks" class="table table-striped table-bordered">
            <%--<tr>--%>
                <%--<td>Date</td>--%>
            <%--</tr>--%>
            <%--<c:forEach var="taskId" items="${allTasksIds}">--%>
                <%--<tr>--%>
                    <%--<td>${taskId} Running... </td>--%>
                    <%--<td><a href="${pageContext.request.contextPath}/tasks/${taskId}/stop">Stop</a></td>--%>
                <%--</tr>--%>
            <%--</c:forEach>--%>
            <%--<tr>--%>
                <%--<td></td>--%>
            <%--</tr>--%>
        </table>

        <div id="tasks_list"/>
    </div>


    <script>
        setInterval(getTasks, 500);


        function getTasks(){
            $.ajax({
                type: "POST",
                url: "/refresh_tasks",
                dataType: 'json',

                success: function(data){
                    $('#table_tasks').html("");
                    if(data) {
                        var tr;
                        for (var i = 0; i < data.length; i++) {
                            var str = [];
                            str.push("<a href=${pageContext.request.contextPath}/tasks/");
                            str.push(data[i] + "/stop>");
                            str.push('<span class="glyphicon glyphicon-off"></span>');
                            str.push("</a>");

                            tr = $('<tr/>');
                            tr.append("<td>" + data[i] + "</td>");
                            tr.append('<td>' + str.join("") + '</td>');
                            $('#table_tasks').append(tr);
                        }
                    }
                }
            });
        }
    </script>
</div>
</body>
</html>
