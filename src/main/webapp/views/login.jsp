<%@ page import="ru.ediweb.panel.entity.enums.Role" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title>Jboss Control Panel</title>
        <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="${pageContext.servletContext.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div class="container" >

        <div class="text-center">
            <h1> Welcome to Jboss Control Panel </h1>
            <p><strong>Please, log in!</strong></p>
        </div>

        <div class="panel panel-default panel-body" style="width: 360px; padding: 20px 30px 10px 30px; margin: 0 auto;">
            <form action="${pageContext.servletContext.contextPath}/login" method="post">
                <div class="form-group">
                    <label for="username">Login</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter your login">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter ypur password">
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Enter</button>

            </form>
        </div>

        <c:if test="${'failed' eq param.auth}">
            <div class="alert alert-danger" style="width: 360px; margin: auto; margin-top: 20px">
                <strong>Login Failed (В авторизации отказано)</strong><br />
                <strong>Reason : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</strong>
            </div>
        </c:if>



    </div>










    </body>
</html>
