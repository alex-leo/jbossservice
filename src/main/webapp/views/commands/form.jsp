<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../resources/js/jquery-ui.min.js"></script>

    <style>
        ul.source, ol.target {
            min-height: 50px;
            margin: 0px 25px 10px 0px;
            padding: 2px;
            border-width: 1px;
            border-style: solid;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            list-style-type: none;
            list-style-position: inside;
        }
        ul.source {
            border-color: #f8e0b1;
        }
        ol.target {
            border-color: #add38d;
        }
        .source li, .target li {
            margin: 5px;
            padding: 5px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
        }
        .source li {
            background-color: #fcf8e3;
            border: 1px solid #fbeed5;
            color: #a47f50;
        }
        .target li {
            background-color: #ebf5e6;
            border: 1px solid #d6e9c6;
            color: #3d7b3d;
            list-style-type: decimal;
        }

        .target ol {
            height: 500px;
        }

        .sortable-dragging {
            border-color: #ccc !important;
            background-color: #fafafa !important;
            color: #bbb !important;
        }
        .sortable-placeholder {
            height: 40px;
        }
        .source .sortable-placeholder {
            border: 2px dashed #f8e0b1 !important;
            background-color: #fefcf5 !important;
        }
        .target .sortable-placeholder {
            border: 2px dashed #add38d !important;
            background-color: #f6fbf4 !important;
        }

        body > li {
            width: 300px;
            margin: 5px;
            padding: 5px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
            list-style-type: none;
            list-style-position: inside;
            border-width: 1px;
            border-style: solid;
            border-color: #ccc !important;
            background-color: #fafafa !important;
            color: #bbb !important;
        }
        .listActive {
            border: 1px solid #ccc;
            background-color: #fcfcfc;
            padding: 0.5em 0 3em 0 !important;
        }
        .placeholder {
            list-style-type: none;
            text-align: center;
            font-style: italic;
            border: 1px dashed #ddd !important;
            background-color: #fff !important;
            color: #aaa !important;
        }
        .dismiss {
            float: right;
            position: relative;
            padding-top: 3px;
            /*line-height: 22px;*/
            /*font-size: 100px;*/
            font-weight: bold;
            text-decoration: none !important;
            color: #468847;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Add command</small></p></h2>
        </div>
        <jsp:include page="../header.jsp" />
    </div>

    <c:choose>
        <c:when test="${actionAdd == true}">
            <c:set var="action" value="${pageContext.request.contextPath}/commands/add"/>
        </c:when>
        <c:otherwise>
            <c:set var="action" value="${pageContext.request.contextPath}/commands/${commandModel.name}/update"/>
        </c:otherwise>
    </c:choose>



    <div class="row center-block col-md-12">
        <form:form class="form-horizontal" id="commandForm" method="POST" onsubmit="saveValues()" action="${action}" modelAttribute="commandModel">
            <div class="form-group">
                <label class="col-sm-4 control-label">Name</label>
                <div class="col-sm-4">
                    <form:input path="name" cssClass="form-control"/>
                </div>
                <form:hidden path="steps" id="commandModelIdList" />
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Has parameters</label>
                <div class="col-sm-4">
                    <form:checkbox path="hasParameters" cssStyle="margin-top: 12px"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2">
                    <form:errors path="name" cssClass="alert alert-danger error-validation-message" />
                </div>
            </div>
        </form:form>
    </div>

    <div class="sideBySide">
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-offset-2">
                <div class="h5"><strong>All steps</strong></div>
                <ul class="source">
                    <c:forEach items="${steps}" var="step">
                        <li id="${step.id}">
                            <div>${step.name}</div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="h5"><strong>Steps in command</strong></div>
                <ol class="target">
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-2" style="margin-bottom: 12px">
                <button class="btn btn-primary btn-sm btn-success" onclick="$('#commandForm').submit();">Save</button>
                <a href="${pageContext.request.contextPath}/commands"><button class="btn btn-primary btn-sm btn-danger">Cancel</button></a>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready ( function(){
            var string = document.getElementById('commandModelIdList').value/*.split(",")*/;

            if(string.length !== 0){
                var arr = string.split(",");

                $(arr).each(function(item){
                    var link = $("<a href='#' class='dismiss'><span class='glyphicon glyphicon-remove'></span></a>");
                    var list = $("<li></li>").text($("#" + arr[item]).text());
                    $(list).attr("id", arr[item]);
                    $(list).append(link);
                    $("ol").append(list);
                });
            }
        });

        $(".source li").draggable({
            addClasses: false,
            appendTo: "body",
            helper: "clone"
        });

        $(".target").droppable({
            addClasses: false,
            activeClass: "listActive",
            accept: ":not(.ui-sortable-helper)",
            drop: function(event, ui) {
                var link = $("<a href='#' class='dismiss'><span class='glyphicon glyphicon-remove'></span></a>");
                var list = $("<li></li>").text(ui.draggable.text());
                $(list).attr("id", ui.draggable.attr('id'));
                $(list).append(link);
                $(list).appendTo(this);
            }
        }).sortable({
            items: "li:not(.placeholder)",
            sort: function() {
                var link = $("<a href='#' class='dismiss'><span class='glyphicon glyphicon-remove'></span></a>");
            }
        }).on("click", ".dismiss", function(event) {
            event.preventDefault();
            $(this).parent().remove();
        });

        function saveValues() {
            var items = [];
            $(".target").each(function(){
                $(this).find("li").each(function(){
                    items.push($(this).attr('id'));
                })
            });
            $('#commandModelIdList').val(items);
        };
    </script>

</div>
</body>
</html>