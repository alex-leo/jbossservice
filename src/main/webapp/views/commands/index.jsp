<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Commands</small></p></h2>
        </div>

        <jsp:include page="../header.jsp" />
    </div>
    <div class="row center-block col-md-12"  style="width: 42%">
        <table class="table table-striped table-bordered">
            <tr>
                <td><strong>Name</strong></td>
                <td><strong>Edit</strong></td>
            </tr>
            <c:forEach var="command" items="${commands}">
                <tr>
                    <td><c:out value="${command.name}"/></td>
                    <td style="width: 24%; text-align: center">
                        <a href="${pageContext.request.contextPath}/commands/${command.name}/edit" class="btn btn-primary btn-sm">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="${pageContext.request.contextPath}/commands/${command.name}/delete" class="btn btn-danger btn-sm">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <!--Добавить комманду-->
    <div class="col-md-12">
        <a href="${pageContext.request.contextPath}/commands/add"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form" style="margin-bottom: 12px">Add command</button></a>
    </div>
</div>
</body>
</html>
