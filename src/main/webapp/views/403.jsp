<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Acces Denied</title>
    <meta charset="utf-8">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
    <div style="height: 80px; background-color: red; margin-bottom: 20px"></div>

    <div style="width: 700px; margin: auto">
        <div class="jumbotron text-center" style="margin: auto; padding: 20px;">
            <h1>Access is Denied!</h1>
            <h2><p>You don't have permissions to access this page!</p>
                <p>- - - - -</p>
                <p>У Вас недостаточно прав для просмотра данной страницы!</p></h2>
        </div>
    </div>



</body>
</html>
