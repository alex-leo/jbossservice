<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:useBean id="dateObject" class="java.util.Date" />

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js" type="text/javascript"></script>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Main page</small></p></h2>
        </div>

        <jsp:include page="header.jsp" />

        <!-- Панель деплоя, пакетного режима -->


        <script>
            $(function() {
                showOrHideParametersField();
            });

            function checkAll(){
                $('#panels :checkbox').prop('checked', true);
            }

            function unCheckAll(){
                $('#panels :checkbox').prop('checked', false);
            }

            function showOrHideParametersField(){
                //Поле ввода параметров (появляется и исчезает)
                var field = document.getElementById('parameters');

                //Получаем текст выбранного элемента из комманд
                var e = document.getElementById("commands");
                var p = e.options[e.selectedIndex].text;

                //Получаем значение true или false для поля параметры
                var isParams = document.getElementById(p).innerHTML;

                //Отображаем, или скрываем поле вводя параметров, в зависимости от значения isParams
                if(isParams == "false") field.style.display = 'none';
                else field.style.display = 'block';
            }

            function setServersList(){
                var serversList = [];

                $('#panels :checkbox:checked').each(function(){
                    serversList.push($(this).attr('id'));
                });

                if(serversList.length == 0){
                    alert("Select servers, please!");
                    return false;
                }

                document.getElementById('servers').value = serversList;
            }
        </script>


        <div class="row">
            <form method="POST" action="/execute"  onsubmit="return setServersList()">
                <sec:authorize access="hasAnyAuthority('ADMIN', 'DEPLOYER')">
                    <!--Панель комманд-->
                    <div class="panel-body" style="padding-left: 0">
                        <div class="col-lg-12">
                            <div class="row ">
                                <div class="col-lg-3">
                                    <select class="form-control input-sm" id="commands" name="command" onchange="showOrHideParametersField()">
                                        <c:forEach items="${commands}" var="command">
                                            <option>${command.name}</option>
                                        </c:forEach>
                                    </select>

                                    <input type="text" name="servers" id="servers" hidden>

                                    <ul id="ul" hidden>
                                        <c:forEach items="${commands}" var="command">
                                            <li id="${command.name}">${command.hasParameters}</li>
                                        </c:forEach>
                                    </ul>

                                </div>

                                <div class="col-lg-3" id="parameters">
                                    <input name="parameters" class="form-control input-sm" type="text" style="width: 264px"/>
                                </div>

                                <div class="col-lg-3">
                                    <button type="submit" class="btn btn-primary btn-sm btn-success">Run command</button>
                                </div>
                                <div style="float: right">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="checkAll()">Check all</button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="unCheckAll()">Uncheck All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </sec:authorize>

                <!--Список серверов-->
                <div id="panels">
                    <c:forEach var="i" items="${servers}">
                        <div class="col-md-4 col-sm-6">
                            <div class="panel panel-default">
                                <!--Верхняя шапка-->
                                <div class="panel-heading small">
                                    <strong>${i.name}</strong>
                                    <input type="checkbox" id="${i.id}" style="float: right"/>
                                </div>
                                <!--Нижнее окно-->
                                <div class="panel-body">
                                    <table style="line-height: 10px" class="table-condensed small">
                                        <tr>
                                            <td><strong>Type:</strong></td>
                                            <td>${i.type.type}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Host:</strong></td>
                                            <td>${i.host}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Path:</strong></td>
                                            <td>${i.path}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>ID:</strong></td>
                                            <td>${i.id}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </form>
        </div>

    </div>
</div>
</body>
</html>
