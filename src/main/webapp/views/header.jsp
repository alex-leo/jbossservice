<%@ page import="ru.ediweb.panel.entity.enums.Role" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
        <sec:authentication var="user" property="principal" />

        <nav class="navbar navbar-default" role="navigation">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row">
                    <div class="col-md-9">
                        <ul class="nav navbar-nav">
                            <sec:authorize access="hasAuthority('ADMIN')">
                                <li><a href="${pageContext.request.contextPath}/">Deploying</a></li>
                                <li><a href="${pageContext.request.contextPath}/users">Users</a></li>
                                <li><a href="${pageContext.request.contextPath}/servers">Servers</a></li>
                                <li><a href="${pageContext.request.contextPath}/server_types">Server types</a></li>
                                <li><a href="${pageContext.request.contextPath}/commands">Commands</a></li>
                                <li><a href="${pageContext.request.contextPath}/steps">Steps</a></li>
                                <li><a href="${pageContext.request.contextPath}/logs">Logs</a></li>
                                <li><a href="${pageContext.request.contextPath}/tasks">Tasks</a></li>
                                <li><a href="${pageContext.request.contextPath}/history">History</a></li>


                                <a style="color: #ffffff" href="${pageContext.request.contextPath}/test">test</a>


                            </sec:authorize>
                        </ul>
                    </div>
                    <div class="col-md-3" style="padding-left: 40px">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/users/${user.username}/change_password">Change password</a></li>
                            <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <noscript>
            <div class="alert alert-danger">
                JavaScript is disabled in your browser! Please, turn it on to have all provided opportunities!
            </div>
        </noscript>
</body>
</html>
