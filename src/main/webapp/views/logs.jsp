<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dateObject" class="java.util.Date" />

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
</head>
<body>

<div class="container" id="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Logs</small></p></h2>
        </div>

        <jsp:include page="header.jsp" />

        <div id="out" style="overflow:auto">
            <table id="table_logs" class="small">
                <tr>
                    <td style="width: 330px"/>
                    <td/>
                </tr>
            </table>
        </div>

        <script>
            setInterval(getNewLogs, 5000);
            var lastTimestmp = 0;

            function getNewLogs(){
                $.ajax({
                    type: "POST",
                    url: "/get_new_logs",
                    dataType: 'json',
                    data: "lastTimestmp=" + lastTimestmp,
                    success: function(data){

                        if(data.length !== 0)lastTimestmp = data[data.length-1].timestmp;
                        console.log(lastTimestmp);

                        $.each(data, function(i){
                            var date = new Date(data[i].timestmp);

                            //checkScrollBottom
                            if((window.innerHeight + window.scrollY) >= document.body.offsetHeight){
                                $('#table_logs tr:last').after("<tr><td>"+ date + "</td><td>" + data[i].formatted_message + "</td></tr>");
                                document.body.scrollTop = document.body.scrollHeight;
                            } else {
                                $('#table_logs tr:last').after("<tr><td>"+ date + "</td><td>" + data[i].formatted_message + "</td></tr>");
                            }
                        });
                    }
                });
            }
        </script>
    </div>
</div>
</body>
</html>
