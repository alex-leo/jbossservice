<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Servers</small></p></h2>
        </div>

        <jsp:include page="../header.jsp" />
    </div>
    <div class="row center-block col-md-12"  <%--style="width: 97%"--%>>
        <table class="table table-striped table-bordered">
            <tr>
                <td><strong>Name</strong></td>
                <td><strong>Type</strong></td>
                <td><strong>Host</strong></td>
                <td><strong>Path</strong></td>
                <td><strong>Edit</strong></td>
            </tr>
            <c:forEach var="server" items="${servers}">
                <tr>
                    <td><c:out value="${server.name}"/></td>
                    <td><c:out value="${server.type.type}"/></td>
                    <td><c:out value="${server.host}"/></td>
                    <td><c:out value="${server.path}"/></td>
                    <td style="width: 10%; text-align: center">
                        <a href="${pageContext.request.contextPath}/servers/${server.name}/edit" class="btn btn-primary btn-sm">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="${pageContext.request.contextPath}/servers/${server.name}/delete" class="btn btn-danger btn-sm">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <!--Добавить сервер-->
    <div class="col-md-12">
        <a href="${pageContext.request.contextPath}/servers/add"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#form" style="margin-bottom: 12px">Add server</button></a>
    </div>
</div>
</body>
</html>
