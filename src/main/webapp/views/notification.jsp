<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">

    <style>
        .notification {
            width: 600px;
            margin: 0 auto;
            margin-top: 120px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">

        <div class="jumbotron notification">
            <h2>
                <div>${successMessage}</div>
                <div style="color: red">${failedMessage}</div>
                <div>${newPassword}</div>
                <div>${updated}</div>
                <div>${password_reset_status}</div>
            </h2>

            <a class="btn btn-sm btn-primary" style="width: 80px" href="${targetPage}">Ok</a>
            
        </div>
    </div>
</body>
</html>
