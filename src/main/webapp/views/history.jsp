<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dateObject" class="java.util.Date" />

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
</head>
<body>

<div class="container" id="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>History</small></p></h2>
        </div>

        <jsp:include page="header.jsp" />



    </div>
</div>
</body>
</html>
