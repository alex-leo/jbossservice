<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap-toggle.min.js"></script>
</head>

<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Change password</small></p></h2>
        </div>

        <jsp:include page="../header.jsp"/>

        <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/users/${login}/change_password">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-offset-1 col-sm-4 control-label">Current password</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" name="currentPassword" id="currentPassword" placeholder="Current password" autocomplete="off">
                    </div>
                    <c:if test="${not empty wrongCurrentPassword}">
                        <div class="alert alert-danger error-validation-message">
                            <c:out value="${wrongCurrentPassword}"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-offset-1 col-sm-4 control-label">New password</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off">
                    </div>
                    <c:if test="${not empty wrongPasswordLength}">
                        <div class="alert alert-danger error-validation-message" style="padding: 8px; margin-bottom: 0;">
                            <c:out value="${wrongPasswordLength}"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-offset-1 col-sm-4 control-label">Confirm new password</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off">
                    </div>
                    <c:if test="${not empty wrongConfirmPassword}">
                        <div class="alert alert-danger error-validation-message" style="padding: 8px; margin-bottom: 0">
                            <c:out value="${wrongConfirmPassword}"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <!--Этот див не виден, если javascript выключен-->
            <div class="row" id="js_hidden" style="display: none">
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        <span id="6char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> 6 Characters Long<br>
                    </div>
                    <div class="col-sm-3">
                        <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
                    </div>
                </div>
            </div>
            <!--Отображает элемент, с помощь js если, если js включен-->
            <script>
                document.getElementById("js_hidden").style.display = "block";
            </script>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                        <button  id="saveButton" type="submit" class="btn btn-primary btn-sm btn-success">Save</button>
                        <a href="${pageContext.request.contextPath}/">
                            <button type="button" class="btn btn-primary btn-sm btn-danger">Cancel</button>
                        </a>
                    </div>
                </div>
            </div>
        </form>

        <script>
            window.onload = function() {
                $("#saveButton").prop("disabled", true);
            }

            $("input[type=password]").keyup(function(){
                if($("#password1").val().length >= 6){
                    $("#6char").removeClass("glyphicon-remove");
                    $("#6char").addClass("glyphicon-ok");
                    $("#6char").css("color","#00A41E");
                }else{
                    $("#6char").removeClass("glyphicon-ok");
                    $("#6char").addClass("glyphicon-remove");
                    $("#6char").css("color","#FF0004");
                }

                if(($("#password1").val() == $("#password2").val()) && ($("#password1").val().length != 0)){
                    $("#pwmatch").removeClass("glyphicon-remove");
                    $("#pwmatch").addClass("glyphicon-ok");
                    $("#pwmatch").css("color","#00A41E");
                }else{
                    $("#pwmatch").removeClass("glyphicon-ok");
                    $("#pwmatch").addClass("glyphicon-remove");
                    $("#pwmatch").css("color","#FF0004");
                }

                if(($("#password1").val().length >= 6) && ($("#password1").val() == $("#password2").val())){
                    $("#saveButton").prop("disabled", false);
                } else {
                    $("#saveButton").prop("disabled", true);
                }
            });
        </script>
    </div><!--/row-->
</div><!--/.container-->
</body>
</html>




