<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js" type="text/javascript"></script>
</head>

<body>
<div class="container">
        <div class="col-xs-12">
            <div>
                <h2><strong>Jboss Control Panel</strong>
                    <p><small>Users</small></p></h2>
            </div>

            <jsp:include page="../header.jsp"/>
        </div><!--/row-->

        <div class="row center-block col-md-12"  <%--style="width: 97%"--%>>
            <table class="table table-striped table-bordered">
                <tr>
                    <td><strong>Login</strong></td>
                    <td><strong>Role</strong></td>
                    <td><strong>Enabled</strong></td>
                    <td><strong>Edit</strong></td>
                </tr>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td><c:out value="${user.login}"/></td>
                        <td><c:out value="${user.role}"/></td>
                        <td>
                            <c:choose>
                                <c:when test="${user.enabled}">
                                    <div><span class="glyphicon glyphicon-ok-circle text-success"></span></div>
                                </c:when>
                                <c:otherwise>
                                    <div><span class="glyphicon glyphicon-ban-circle text-danger"></span></div>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 10%; text-align: center">
                                <a href="${pageContext.request.contextPath}/users/${user.login}/edit" class="btn btn-primary btn-sm">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="${pageContext.request.contextPath}/users/${user.login}/delete" class="btn btn-danger btn-sm">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <a href="${pageContext.request.contextPath}/users/add" class="btn btn-primary btn-sm">Add User</a>
        </div>



</div><!--/.container-->
</body>
</html>
