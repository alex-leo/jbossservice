<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Jboss Control Panel</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap-toggle.min.js"></script>
</head>

<body>
<div class="container">
    <div class="col-xs-12">
        <div>
            <h2><strong>Jboss Control Panel</strong>
                <p><small>Edit user</small></p></h2>
        </div>

        <jsp:include page="../header.jsp"/>

        <c:choose>
            <c:when test="${actionAdd == true}">
                <c:set var="action" value="${pageContext.request.contextPath}/users/add"/>
            </c:when>
            <c:otherwise>
                <c:set var="action" value="${pageContext.request.contextPath}/users/${user.login}/update"/>
            </c:otherwise>
        </c:choose>

        <!--Edit user form-->
        <form:form class="form-horizontal" modelAttribute="user" method="post" action="${action}">
            <div class="form-group">
                <form:hidden path="id"/>
                <label class="col-sm-4 control-label">Login</label>
                <div class="col-sm-4">
                    <c:choose>
                        <c:when test="${actionAdd == true}">
                            <form:input path="login" cssClass="form-control"/>
                        </c:when>
                        <c:otherwise>
                            <form:input readonly="true" path="login" cssClass="form-control"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2">
                    <form:errors path="login" cssClass="alert alert-danger error-validation-message" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Role</label>
                <div class="col-sm-4">
                    <form:select class="form-control" path="role">
                        <form:options/>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Enabled</label>
                <div class="col-sm-4">
                    <form:checkbox data-toggle="toggle" path="enabled" data-size="small"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-4">
                    <form:button type="submit" class="btn btn-primary btn-sm btn-success">Save</form:button>
                    <a class="btn btn-primary btn-sm btn-danger" href="${pageContext.request.contextPath}/users/">Cancel</a>
                    <c:if test="${empty actionAdd}">
                        <a class="btn btn-primary btn-sm btn-primary pull-right" href="${pageContext.request.contextPath}/users/${user.login}/reset_password">Reset password</a>
                    </c:if>
                </div>
            </div>
        </form:form>
    </div><!--/row-->
</div><!--/.container-->
</body>
</html>




